import requests
from tqdm import tqdm
import json
import time
import re
import pandas as pd
from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

categories = []

root_base = "https://www.nhaccuatui.com/playlist/tags/"
chrome_options = Options()
chrome_options.add_argument("--incognito")
chrome_options.add_argument("--window-size=1920x1080")
driver = webdriver.Chrome(chrome_options=chrome_options, executable_path="./selenium/etc/chromedriver")
driver.get(root_base)
time.sleep(3)

targets = driver.find_elements(By.CSS_SELECTOR, 'ul.detail_menu_browsing_dashboard')

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
for data in targets[2:]:
    try: 
        a_tags = data.find_elements(By.TAG_NAME, "a")
    except: 
        continue
    
    for tag in a_tags: 
        try: 
            href = tag.get_attribute("href")
            title = tag.get_attribute("title")
        except: 
            continue
        
        item_category = dict(
            name = title, 
            url = href, 
            albums = []
        )
        
        # get list of album
        # loop page
        for i in tqdm(range(1, 51, 1), desc=f"{title}: "): 
            url_page = f"{href}?page={i}"
            r = requests.get(url_page, headers=headers)
            soup = BeautifulSoup(r.text, 'html.parser')
            links = soup.find_all("a", class_="box_absolute")
            
            if len(links) == 0: 
                break 
            
            for album in links: 
                item_album = dict(
                    name = album.get("title"), 
                    href =  album.get("href")
                )
                item_category["albums"].append(item_album)
        
        categories.append(item_category)
        

json_obj = json.dumps(categories, indent=4)

# writing to sample.json
with open("./selenium/data/category_music_2.json", "w") as outfile: 
    outfile.write(json_obj)
driver.close()

