
import requests
from tqdm import tqdm
import re
import os
from bs4 import BeautifulSoup
import json 

category_path = "./selenium/data/category_music_2.json"
with open(category_path, "r") as openfile: 
    json_obj = json.load(openfile)

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
def append_to_list(songs, data): 
    if len(songs) == 0: 
        songs.append(data)
        return songs
    
    check = False
    for idx, item in enumerate(songs): 
        if item["url"] == data["url"]: 
            albums = songs[idx]["album"]
            albums.append(data["album"][0])
            albums = list(set(albums))
            songs[idx]["album"] = albums
            
            categories = songs[idx]["category"]
            categories.append(data["category"][0])
            categories = list(set(categories))
            songs[idx]["category"] = categories
            
            check = True
            break
        
    if not check: 
        songs.append(data)
        return songs
    
    return songs


songs = []
song_path = "./selenium/data/song1.json"
if os.path.exists(song_path): 
    with open(song_path, "r") as openfile: 
        songs = json.load(openfile)

for category in json_obj:
    category_name = category["name"]

    print(category_name)

    for album in category["albums"]:
        album_name = album["name"]
        
        try:
            r = requests.get(album["href"], headers=headers)
            soup = BeautifulSoup(r.text, 'html.parser')
            songs_div_tag = soup.find_all("div", id="idScrllSongInAlbum")[0]
        except: 
            # out of index
            continue
        
        for item in tqdm(songs_div_tag.find_all("div", class_="item_content"), desc=f"[{category_name}][{album_name}] : "):
            a_name_song = item.find_all("a", class_="name_song")
            a_name_singers = item.find_all("a", class_="name_singer")

            name_song = a_name_song[0].text.strip()
            href_song = a_name_song[0].get("href")
            name_singers = [a_singer.text.strip() for a_singer in a_name_singers]
            
            item_song = dict(
                name = name_song, 
                url = href_song,
                artist = name_singers, 
                category = [category_name],
                album = [album_name], 
            )
            
            songs = append_to_list(songs, item_song)

save_obj = json.dumps(songs, indent=4, ensure_ascii=False)
save_song_path = "./selenium/data/all_song_2.json"
with open(save_song_path, "w") as outfile: 
    outfile.write(save_obj)
    