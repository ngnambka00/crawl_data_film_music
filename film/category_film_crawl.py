categories = {
    "Phim 18+": [
        "https://ophimmoi.net/category/18/",
        "https://ophimmoi.net/category/phim-18+/"
    ],
    
    "Ác quỷ": [
        "https://ophimmoi.net/category/ac-quy/"
    ],
    
    "Âm nhạc": [
        "https://ophimmoi.net/category/am-nhac/"
    ],
    
    "Ấn độ hình sự": [
        "https://ophimmoi.net/category/an-do-hinh-su/"
    ],

    "Anime": [
        "https://ophimmoi.net/category/anime/"
    ],

    "Bí ẩn": [
        "https://ophimmoi.net/category/bi-an/"
    ],

    "Bí ẩn - Siêu nhân": [
        "https://ophimmoi.net/category/bi-an-sieu-nhien/"
    ],

    "Bom tấn": [
        "https://ophimmoi.net/category/bom-tan/"
    ],

    "Chiến tranh": [
        "https://ophimmoi.net/category/chien-tranh/"
    ],

    "Chiến tranh, chiếu rạp": [
        "https://ophimmoi.net/category/chien-tranhchieu-rap/"
    ],

    "Chiếu rạp": [
        "https://ophimmoi.net/category/chieu-rap/"
    ],

    "Chính kịch": [
        "https://ophimmoi.net/category/chinh-kich/"
    ],

    "Chính kịch - Drama": [
        "https://ophimmoi.net/category/chinh-kich-drama/"
    ],

    "Other": [
        "https://ophimmoi.net/category/chua-phan-loai/"
    ],

    "Clip ngắn": [
        "https://ophimmoi.net/category/clip-ngan/"
    ],

    "CN Animation": [
        "https://ophimmoi.net/category/cn-animation/"
    ],

    "Cổ trang": [
        "https://ophimmoi.net/category/co-trang/"
    ],

    "Cung đấu": [
        "https://ophimmoi.net/category/cung-dau/"
    ],

    "Dã sữ": [
        "https://ophimmoi.net/category/da-su/"
    ],

    "Đam Mỹ": [
        "https://ophimmoi.net/category/dam-my/"
    ],

    "Đời thường": [
        "https://ophimmoi.net/category/doi-thuong/"
    ],

    "Drama": [
        "https://ophimmoi.net/category/drama/"
    ],

    "Drama Chính Kịch": [
        "https://ophimmoi.net/category/drama-chinh-kich/"
    ],

    "Ecchi": [
        "https://ophimmoi.net/category/ecchi/"
    ],

    "Game": [
        "https://ophimmoi.net/category/game/",
        "https://ophimmoi.net/category/game-dien-thoai/",
        "https://ophimmoi.net/category/game-pc/",
        "https://ophimmoi.net/category/giai-dau/"
    ],

    "Gameshow": [
        "https://ophimmoi.net/category/gameshow/"
    ],

    
    "Gây cấn": [
        "https://ophimmoi.net/category/gay-can/"
    ],
    
    "Gia đình": [
        "https://ophimmoi.net/category/gia-dinh-2/","https://ophimmoi.net/category/gia-dinh/",
        "https://ophimmoi.net/category/gia-dinh-hoc-duong/"
    ],
    
    "Giả tưởng":[
        "https://ophimmoi.net/category/gia-tuong/"
    ],
    
    "Giật Gân": [
        "https://ophimmoi.net/category/giat-gan/"
    ],

    "Hài Hước": [
        "https://ophimmoi.net/category/hai/"
        "https://ophimmoi.net/category/hai-huoc/"
    ],
    
    
    "Hài Hước Viễn Tưởng": [
        "https://ophimmoi.net/category/hai-huoc-vien-tuong/"
    ],
    
    "Hàn Quốc": [
        "https://ophimmoi.net/category/han-quoc/"
    ],

    "Hành Động": [
        "https://ophimmoi.net/category/hanh-dong/"
    ],

    "Hành Động Người Lớn": [
        "https://ophimmoi.net/category/hanh-dong-nguoi-lon/"
    ],

    "Hành Động Tâm Lý": [
        "https://ophimmoi.net/category/hanh-dong-tam-ly/"
    ],

    "Hành Động Thể Thao Âm Nhạc": [
        "https://ophimmoi.net/category/hanh-dong-the-thao-am-nhac/"
    ],

    "Hình Sự": [
        "https://ophimmoi.net/category/hinh-su/"
    ],

    "Hoạt Hình": [
        "https://ophimmoi.net/category/hoat-hinh/"
    ],

    "Hoạt Hình Anime": [
        "https://ophimmoi.net/category/hoat-hinh-anime/"
    ],

    "Hoạt Hình Phiêu Lưu": [
        "https://ophimmoi.net/category/hoat-hinh-phieu-luu/"
    ],

    "Học Đường": [
        "https://ophimmoi.net/category/hoc-duong/"
    ],

    "Hồi Hộp": [
        "https://ophimmoi.net/category/hoi-hop/"
    ],

    "Khoa Học": [
        "https://ophimmoi.net/category/khoa-hoc/"
    ],

    "Kịch Tính": [
        "https://ophimmoi.net/category/kich-tinh/"
    ],

    "Kids": [
        "https://ophimmoi.net/category/kids/"
    ],

    "Kiếm Hiệp": [
        "https://ophimmoi.net/category/kiem-hiep/"
    ],

    "Kinh Dị": [
        "https://ophimmoi.net/category/kinh-di/"
    ],

    "Kinh Dị - Ma": [
        "https://ophimmoi.net/category/kinh-di-ma/"
    ],

    "Kinh Dị Người Lớn": [
        "https://ophimmoi.net/category/kinh-di-nguoi-lon/"
    ],

    "Kinh Dị Viễn Tưởng": [
        "https://ophimmoi.net/category/kinh-di-vien-tuong/"
    ],

    "Kinh Điển": [
        "https://ophimmoi.net/category/kinh-dien/"
    ],

    "Lãng mạn": [
        "https://ophimmoi.net/category/lang-man/"
    ],

    "Lgbtq+": [
        "https://ophimmoi.net/category/lgbtq+/"
    ],

    "Lịch sử": [
        "https://ophimmoi.net/category/lich-su/"
    ],

    "Ma": [
        "https://ophimmoi.net/category/ma/"
        "https://ophimmoi.net/category/ma-kinh-di/"
    ],


    "Mới Cổ Trang": [
        "https://ophimmoi.net/category/moi-co-trang/"
    ],

    "Movie & OVA": [
        "https://ophimmoi.net/category/movie-ova/"
    ],

    "Mỹ": [
        "https://ophimmoi.net/category/my/"
    ],

    "Netflix": [
        "https://ophimmoi.net/category/netflix/"
    ],

    "Người Lớn": [
        "https://ophimmoi.net/category/nguoi-lon/"
    ],

    "Nhạc Kịch": [
        "https://ophimmoi.net/category/nhac-kich/"
    ],

    "Nhật Bản": [
        "https://ophimmoi.net/category/nhat-ban/"
    ],

    "ONA": [
        "https://ophimmoi.net/category/ona/"
    ],

    "Phép Thuật": [
        "https://ophimmoi.net/category/phep-thuat/"
    ],

    "Phiêu Lưu": [
        "https://ophimmoi.net/category/phieu-luu/"
    ],

    "Phim Bộ": [
        "https://ophimmoi.net/category/phim-bo/"
    ],

    "Phim Châu Á Kinh Điển": [
        "https://ophimmoi.net/category/phim-chau-a-kinh-dien/"
    ],

    "Phim Lẻ": [
        "https://ophimmoi.net/category/phim-le/"
    ],

    "Phim Lẻ,Chiếu Rạp": [
        "https://ophimmoi.net/category/phim-lechieu-rap/"
    ],

    "Phim Mới": [
        "https://ophimmoi.net/category/phim-moi/",
        "https://ophimmoi.net/category/moi/"
    ],

    "Phim Tvb": [
        "https://ophimmoi.net/category/phim-tvb/"
    ],

    "Quân Đội": [
        "https://ophimmoi.net/category/quan-doi/"
    ],

    "Sắp chiếu": [
        "https://ophimmoi.net/category/sap-chieu/"
    ],

    "Shoujo": [
        "https://ophimmoi.net/category/shoujo/"
    ],

    "Shounen": [
        "https://ophimmoi.net/category/shounen/"
    ],

    "Shounen Ai": [
        "https://ophimmoi.net/category/shounen-ai/"
    ],

    "Siêu Năng Lực": [
        "https://ophimmoi.net/category/sieu-nang-luc/"
    ],

    "Siêu Nhiên": [
        "https://ophimmoi.net/category/sieu-nhien/"
    ],

    "Tài liệu": [
        "https://ophimmoi.net/category/tai-lieu/"
    ],

    "Tâm Lý": [
        "https://ophimmoi.net/category/tam-ly/"
    ],

    "Tâm Lý Tình Cảm": [
        "https://ophimmoi.net/category/tam-ly-tinh-cam/"
    ],

    "Tâm Lý Chiếu Rạp": [
        "https://ophimmoi.net/category/tam-lychieu-rap/"
    ],

    "Thái Lan": [
        "https://ophimmoi.net/category/thai-lan/"
    ],

    "Thảm Họa": [
        "https://ophimmoi.net/category/tham-hoa/"
    ],

    "Thần Thoại": [
        "https://ophimmoi.net/category/than-thoai/"
    ],

    "Thể Loại Khác": [
        "https://ophimmoi.net/category/the-loai-khac/"
    ],

    "Thể Thao": [
        "https://ophimmoi.net/category/the-thao/"
    ],

    "Thể Thao Âm Nhạc": [
        "https://ophimmoi.net/category/the-thao-am-nhac/"
    ],

    "Thiếu Nhi": [
        "https://ophimmoi.net/category/thieu-nhi/"
    ],

    "Thuyết Minh": [
        "https://ophimmoi.net/category/thuyet-minh/"
    ],

    "Tiên Hiệp": [
        "https://ophimmoi.net/category/tien-hiep/"
    ],

    "Tình Cảm": [
        "https://ophimmoi.net/category/tinh-cam/"
    ],

    "Tình Cảm Lãng Mạn": [
        "https://ophimmoi.net/category/tinh-cam-lang-man/"
    ],

    "Tình Yêu": [
        "https://ophimmoi.net/category/tinh-yeu/"
    ],

    "Tội Phạm": [
        "https://ophimmoi.net/category/toi-pham/"
    ],

    "Trọng Sinh": [
        "https://ophimmoi.net/category/trong-sinh/"
    ],

    "Truyền Hình": [
        "https://ophimmoi.net/category/truyen-hinh/"
    ],

    "Tư Liệu": [
        "https://ophimmoi.net/category/tu-lieu/"
    ],

    "TV Show": [
        "https://ophimmoi.net/category/tv-show/"
        "https://ophimmoi.net/category/tv-shows/"
    ],

    "Viễn Tây": [
        "https://ophimmoi.net/category/vien-tay/"
    ],

    "Viễn tưởng": [
        "https://ophimmoi.net/category/vien-tuong/"
    ],
    
    "Võ thuật": [
        "https://ophimmoi.net/category/vo-thuat/"
    ],

    "Vũ trụ": [
        "https://ophimmoi.net/category/vu-tru/"
    ]
}


import requests
from tqdm import tqdm
import re
from bs4 import BeautifulSoup
import json 

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

result_categories = []
for key, hrefs_value in categories.items(): 

    cate_item = {
        "name": key, 
        "list_url_firm": []
    }
    
    for url in tqdm(hrefs_value, desc=key): 
        # add page
        for i in range(1, 200, 1): 
            url = f"{url}page/{i}/"
            
            r = requests.get(url, headers=headers)
            soup = BeautifulSoup(r.text, 'html.parser')
            
            # get url film
            list_film_tag = soup.find_all("ul", class_="last-film-box")
            if len(list_film_tag) == 0: 
                continue
            
            cate_item["list_url_firm"].extend([a_item.get("href") for a_item in list_film_tag[0].find_all("a", class_="movie-item")])


    result_categories.append(cate_item)



save_obj = json.dumps(result_categories, indent=4, ensure_ascii=False)
# writing to sample.json
with open(f"./selenium/data/category_film.json", "w") as outfile: 
    outfile.write(save_obj)

