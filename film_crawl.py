
import requests
from tqdm import tqdm
import re
from bs4 import BeautifulSoup
import json 

with open("./selenium/data/category_film.json", "r") as openfile: 
    json_obj = json.load(openfile)

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

def unit2num(data: str) -> float:
    data = data.lower()

    scaled = 1
    if ord(data[-1]) in range(ord('a'), ord('z') + 1):
        if data[-1] == 'm':
            scaled = 1000000
        if data[-1] == 'k':
            scaled = 1000

        return float(data[:-1]) * scaled

    return float(data)

def append_to_list(films, data): 
    if len(films) == 0: 
        films.append(data)
        return films
    
    check = False
    for idx, item in enumerate(films): 
        if item["url"] == data["url"]: 
            categories = films[idx]["category"]
            categories.append(data["category"][0])
            categories = list(set(categories))
            films[idx]["category"] = categories
            
            check = True
            break
        
    if not check: 
        films.append(data)
        return films
    
    return films

films = []
for category in json_obj:
    category_name = category["name"]
    
    for url_film in tqdm(category["list_url_firm"], desc=f"{category_name} "): 

        try:
            r = requests.get(url_film, headers=headers)
            soup = BeautifulSoup(r.text, 'html.parser')
            film_info_tag = soup.find_all("div", class_="block-movie-info movie-info-box")[0]
            actor_tag = soup.find_all("div", class_="block-actors")[0]
            actor_tag = soup.find_all("ul", id="list_actor_carousel")[0]
            
            item_film = dict(
                name = film_info_tag.find_all("span", class_="title-2")[0].text,
                sub_name = film_info_tag.find_all("span", class_="title-1")[0].text,
                year = int(film_info_tag.find_all("span", class_="title-year")[0].text[1:-1]),
                
                actor = [actor_tag.text for actor_tag in actor_tag.find_all("span", class_="actor-name-a")],
                category = [category_name],
                url = url_film,
                
                country = "",
                director = "",
                duration = 0,
                view = 0
            )
            
            dt_text_tags = [in_tag.text for in_tag in film_info_tag.find_all("dt", class_="movie-dt")]
            dd_tags = film_info_tag.find_all("dd", class_="movie-dd")
            
            for idx, text_tag in enumerate(dt_text_tags):
                # dạo diễn - director 
                if text_tag == "Đạo diễn:": 
                    item_film["director"] = dd_tags[idx].find_all("a", class_="director")[0].text
                
                # quốc gia
                if text_tag == "Quốc gia:": 
                    item_film["country"] = dd_tags[idx].find_all("a")[0].text
                
                # thời lượng
                if text_tag == "Thời lượng:":
                    item_film["duration"] = int(dd_tags[idx].text.strip().split(" ")[0])
                
                # Lượt xem
                if text_tag == "Lượt xem:": 
                    item_film["view"] = unit2num(dd_tags[idx].text.strip())

            films = append_to_list(films, item_film)
            
        except:
            continue
        

save_obj = json.dumps(films, indent=4, ensure_ascii=False)
# writing to sample.json
with open(f"./selenium/data/all_film.json", "w") as outfile: 
    outfile.write(save_obj)
        