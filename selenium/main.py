"""
    file code 
    etc để lưu driver chrome
    data để lưu data lúc crawl
"""

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import datetime
import os, time
import pandas as pd 

if __name__ == "__main__": 

# iframe 2: index 1
# class city
# class total
# class daynow
# class die
# fullpath Xpath /html/body/div[1]/div[2]/div[3]/div/iframe
    
    chrome_options = Options()
    chrome_options.add_argument("--incognito")
    chrome_options.add_argument("--window-size=1920x1080")
    driver = webdriver.Chrome(chrome_options=chrome_options, executable_path="./etc/chromedriver")
    
    url = "https://covid19.gov.vn/"
    driver.get(url)
    time.sleep(3)
    
    # First, switch to frame which contains information tag
    driver.switch_to.frame(1)
    targets = driver.find_elements(By.XPATH, '/html/body/div[2]/div[1]/div')
    for idx, data in enumerate(targets):
        cities = data.find_elements(By.CLASS_NAME, "city")
        totals = data.find_elements(By.CLASS_NAME, "total")
        todays = data.find_elements(By.CLASS_NAME, "daynow")
        deads = data.find_elements(By.CLASS_NAME, "die")
    
    cities.pop(0)
    totals.pop(0)
    todays.pop(0)
    deads.pop(0)
    
    list_cities = [city.text for city in cities]
    list_total = [total.text for total in totals]
    list_today = [today.text for today in todays]
    list_dead = [dead.text for dead in deads]
    
    result_df = pd.DataFrame({
        "city": list_cities, 
        "total": list_total, 
        "day_now": list_today, 
        "dead": list_dead
    })
    
    result_df.to_csv("./data/result.csv", index=False)
    # print("[CHECK]")
    # print(result_df)
    
    # elements = driver.find_elements(By.CSS_SELECTOR, ".storylink")
    # storyTitles = [el.text for el in elements]
    # storyUrls = [el.get_attribute("href") for el in elements]
    
    # elements = driver.find_elements(By.CSS_SELECTOR, ".score")
    # scores = [el.text for el in elements]
    
    # elements = driver.find_elements(By.CSS_SELECTOR, ".sitebit a")
    # sites = [el.get_attribute("href") for el in elements]

    # print("[CHECK]")
    # print(storyTitles)
    # print(storyUrls)
    # print(scores)
    # print(sites)
    
    driver.close()

