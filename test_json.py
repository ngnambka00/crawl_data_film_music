import json

# 1. Write
# dictionary = {
#     "name": "sathiyajith",
#     "rollno": 56,
#     "cgpa": 8.6,
#     "phonenumber": "9976770500"
# }

# json_obj = json.dumps(dictionary, indent=4)

# print(json_obj)
# # writing to sample.json
# with open("./selenium/data/sample.json", "w") as outfile: 
#     outfile.write(json_obj)


# 2. Read
with open("./selenium/data/category_music_2.json", "r") as openfile: 
    json_obj = json.load(openfile)

print(json_obj[0])
# print(type(json_obj))