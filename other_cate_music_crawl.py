import requests
from tqdm import tqdm
import json
from bs4 import BeautifulSoup

categories = {
    "Sinh nhật": "https://www.nhaccuatui.com/playlist/tags/sinh-nhat",
    "Mưa": "https://www.nhaccuatui.com/playlist/tags/mua",
    "Mùa đông": "https://www.nhaccuatui.com/playlist/tags/mua-dong",
    "Yên Tĩnh": "https://www.nhaccuatui.com/playlist/tags/yen-tinh",
    "Spa Yoga": "https://www.nhaccuatui.com/playlist/tags/spa-yoga",
    "Làm việc nhà": "https://www.nhaccuatui.com/playlist/tags/lam-viec-nha",
    "Nắng": "https://www.nhaccuatui.com/playlist/tags/nang",
    "Xế chiều": "https://www.nhaccuatui.com/playlist/tags/xe-chieu",
    "Bất hủ": "https://www.nhaccuatui.com/playlist/tags/bat-hu",
    "Cover": "https://www.nhaccuatui.com/playlist/tags/cover",
    "Chill out": "https://www.nhaccuatui.com/playlist/tags/chill-out",
    "Maskup": "https://www.nhaccuatui.com/playlist/tags/mashup",
    "Nhạc hot tháng": "https://www.nhaccuatui.com/playlist/tags/nhac-hot-thang",
    "Song ca": "https://www.nhaccuatui.com/playlist/tags/song-ca",
    "Giải thưởng âm nhạc": "https://www.nhaccuatui.com/playlist/tags/giai-thuong-am-nhac",
    "Top 100": "https://www.nhaccuatui.com/playlist/tags/top-100",
    "Khiêu vũ": "https://www.nhaccuatui.com/playlist/tags/khieu-vu",
    "Band": "https://www.nhaccuatui.com/playlist/tags/band",
    "Giáng sinh": "https://www.nhaccuatui.com/playlist/tags/giang-sinh",
    "Cha": "https://www.nhaccuatui.com/playlist/tags/cha",
    "Gia đình": "https://www.nhaccuatui.com/playlist/tags/gia-dinh",
    "Tình bạn": "https://www.nhaccuatui.com/playlist/tags/tinh-ban",
    "Epic Music": "https://www.nhaccuatui.com/playlist/tags/epic-music",
    "Đoàn - Đảng": "https://www.nhaccuatui.com/playlist/tags/doan-dang",
    "Baroque": "https://www.nhaccuatui.com/playlist/tags/baroque",
    "Tết": "https://www.nhaccuatui.com/playlist/tags/tet",
    "Mẹ": "https://www.nhaccuatui.com/playlist/tags/me",
    "Con gái": "https://www.nhaccuatui.com/playlist/tags/con-gai",
    "Tình yêu": "https://www.nhaccuatui.com/playlist/tags/tinh-yeu",
    "Hoạt hình": "https://www.nhaccuatui.com/playlist/tags/hoat-hinh",
    "Cho Những Chuyến Đi": " https://www.nhaccuatui.com/playlist/tags/cho-nhung-chuyen-di",
    "Weekend": "https://www.nhaccuatui.com/playlist/tags/weekend",
    "Bé": "https://www.nhaccuatui.com/playlist/tags/be",
    "Con trai": "https://www.nhaccuatui.com/playlist/tags/con-trai",
    "Thầy cô": "https://www.nhaccuatui.com/playlist/tags/thay-co",
}


headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
result_categories = []

for category_name, category_url in categories.items(): 
    item_category = dict(
        name = category_name, 
        url = category_url, 
        albums = []
    )
    
    # get list of album
    # loop page
    for i in tqdm(range(1, 51, 1), desc=f"{category_name}: "): 
        url_page = f"{category_url}?page={i}"
        r = requests.get(url_page, headers=headers)
        soup = BeautifulSoup(r.text, 'html.parser')
        links = soup.find_all("a", class_="box_absolute")
        
        if len(links) == 0: 
            break 
        
        for album in links: 
            item_album = dict(
                name = album.get("title"), 
                href =  album.get("href")
            )
            item_category["albums"].append(item_album)
    
    result_categories.append(item_category)
    
json_obj = json.dumps(result_categories, indent=4, ensure_ascii=False)
# writing to sample.json
with open("./selenium/data/category_music_2.json", "w") as outfile: 
    outfile.write(json_obj)