
import requests
from tqdm import tqdm
import re
import pandas as pd
from bs4 import BeautifulSoup

domain_base = "https://www.bluegrassrealtors.com/"

link_base = "https://www.bluegrassrealtors.com/index.php?\
src=directory&view=rets_flex_active_agents&srctype=rets_flex_active_agents_lister\
&xsearch_id=rets_flex_active_agents_alpha&xsearch=dummy&query=name.starts."

suffix_pos = '&pos=0,1000,1000'

list_link = []
for one in range(97, 123): 
    list_link.append(link_base + chr(one) + suffix_pos)
    
# for item in list_link: 
#     # Phân tích cú pháp 
    
#     # Lưu file csv
#     pass

# add header requests like chrome
columns_name = ["profile_link", "name", "desc", "addr", "email", "phone"]
result_df = pd.DataFrame(columns=columns_name)

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
for i in range(len(list_link)):
    link = list_link[i]
    r = requests.get(link, headers=headers)
    soup = BeautifulSoup(r.text, 'html.parser')
    tb_data = soup.find_all("table", class_="stack striped directoryTable")
    tr_data = tb_data[0].find_all("tr")
    
    for row in tqdm(tr_data[1:], desc=f"{i + 1} phase ... "): 
        td_list = row.find_all("td")
        
        # find profile_link
        try: 
            link_profile = domain_base + td_list[0].find_all('a')[0].get('href')
        except: 
            link_profile = ""
            
        # find name & desc
        try: 
            name = td_list[1].text
            names = name.split("\n")
            name1 = names[1].strip()
            name2 = names[2].strip()
        except: 
            name1 = ""
            name2 = ""
        
        # find addr
        try:
            addr = td_list[2].text.strip().replace(",", "-").replace("\n", "")
            addr = re.sub(" +", " ", addr)
        except: 
            addr = ""
        
        # find email
        try: 
            email = td_list[3].find_all('a')[0].text.strip()
        except: 
            email = ""

        # fine phone
        try: 
            phone_pattern = "\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4}"
            p = re.compile(phone_pattern)
            x = p.search(td_list[3].text)
            phone = str(x.group(0))
        except: 
            phone = ""
        
        entity_col = pd.DataFrame([[link_profile, name1, name2, addr, email, phone]], columns=columns_name)
        result_df = result_df.append(entity_col)
        

result_df.to_excel("./selenium/data/bluegrassrealtors.xlsx", index=False)